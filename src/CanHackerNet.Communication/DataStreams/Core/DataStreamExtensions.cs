﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace CanHackerNet.Communication.DataStreams.Core
{
    public static class DataStreamExtensions
    {
        public static async Task<T> WithTimeout<T>(this Task<T> task, int timeout)
        {
            var cts = new CancellationTokenSource(timeout);
            var tcs = new TaskCompletionSource<bool>();

            using (cts.Token.Register(
                s =>
                ((TaskCompletionSource<bool>)s).TrySetResult(true), tcs))
            {
                if (task != await Task.WhenAny(task, tcs.Task))
                {
                    throw new OperationCanceledException();
                }
            }

            return await task;
        }
    }
}
