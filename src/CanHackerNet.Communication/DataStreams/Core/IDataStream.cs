﻿using System;
using System.Threading.Tasks;

namespace CanHackerNet.Communication.DataStreams.Core
{
    public interface IDataStream : IDisposable
    {
        event EventHandler<string> OnDataReceived;

        Task OpenDataSteram(string streamName);

        Task CloseDataStream();

        Task WriteData(byte[] data);

        Task<string> ReadData();

        Task<string> ReadData(int timeout);

        void StartDataStreaming();

        void StopDataStreaming();
    }
}
