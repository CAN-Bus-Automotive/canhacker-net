﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using CanHackerNet.Communication.DataStreams.Core;
using CanHackerNet.Communication.Protocols.Lawicel;
using HwSerialPort = System.IO.Ports.SerialPort;

namespace CanHackerNet.Communication.DataStreams.SerialPort
{
    [ExcludeFromCodeCoverage]
    public class SerialPortDataStream : IDataStream
    {
        private const string _endOfPacket = "\r";
        private string _streamName = string.Empty;
        private CancellationTokenSource _cts = new CancellationTokenSource();
        private CancellationToken _ct;

        private HwSerialPort _serialPort = new HwSerialPort();

        ~SerialPortDataStream()
        {
            Dispose();
        }

        public event EventHandler<string> OnDataReceived;

        public bool IsOpen { get; private set; }

        public Task<string> ReadData()
        {
            if (!_serialPort.IsOpen)
            {
                throw new InvalidOperationException("Open stream before writing");
            }

            var packet = _serialPort.ReadTo(_endOfPacket);
            return Task.FromResult(packet);
        }

        public Task WriteData(byte[] data)
        {
            if (!_serialPort.IsOpen)
            {
                throw new InvalidOperationException("Open stream before writing");
            }

            return _serialPort.BaseStream.WriteAsync(data, 0, data.Length);
        }

        public Task OpenDataSteram(string streamName)
        {
            if (string.IsNullOrEmpty(streamName))
            {
                throw new ArgumentException("You must set stream name");
            }

            if (_serialPort.IsOpen)
            {
                throw new InvalidOperationException("Close active stream before opening new");
            }

            _streamName = streamName;

            _serialPort = new HwSerialPort(_streamName);
            _serialPort.Open();
            IsOpen = true;

            return Task.CompletedTask;
        }

        public Task CloseDataStream()
        {
            if (_serialPort.IsOpen)
            {
                _cts?.Cancel();
                _serialPort.Close();
                IsOpen = false;
            }

            return Task.CompletedTask;
        }

        public async Task<string> ReadData(int timeout)
        {
            byte[] data = new byte[1];

            string response = string.Empty;

            while (data[0] != LawicelCommands.CR)
            {
                try
                {
                    await _serialPort.BaseStream.ReadAsync(data, 0, 1).WithTimeout(timeout);
                    response += Convert.ToChar(data[0]);
                }
                catch (OperationCanceledException)
                {
                    throw new TimeoutException("Timeout. No answer from device.");
                }
            }

            return response;
        }

        public void Dispose()
        {
            CloseDataStream();
        }

        public void StartDataStreaming()
        {
            Task.Factory.StartNew(
                  () =>
                  {
                      while (!_ct.IsCancellationRequested)
                      {
                          var packet = _serialPort.ReadTo(_endOfPacket);
                          OnDataReceived?.Invoke(this, packet);
                      }
                  }, _ct);
        }

        public void StopDataStreaming()
        {
            _cts?.Cancel();
        }
    }
}
