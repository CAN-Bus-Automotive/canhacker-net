﻿using System;
using System.Text;
using System.Threading.Channels;
using System.Threading.Tasks;
using CanHackerNet.Communication.DataStreams.Core;
using CanHackerNet.Communication.DataStreams.SerialPort;
using CanHackerNet.Communication.Protocols.Core;

namespace CanHackerNet.Communication.Protocols.Lawicel
{
    public class Lawicel : ICan
    {
        private readonly IDataStream _dataStream;

        public Lawicel(IDataStream dataStream)
        {
            _dataStream = dataStream;
            _dataStream.OnDataReceived += DataStream_OnDataReceived;
        }

        public event EventHandler<string> PackageReceived;

        public Channel<string> DataChannel => Channel.CreateUnbounded<string>();

        public Task OpenDataStream(object settings)
        {
            var streanSettings = (SerialPortDataStreanSettings)settings;
            return _dataStream.OpenDataSteram(streanSettings.PortName);
        }

        public Task CloseDataStream()
        {
            return _dataStream.CloseDataStream();
        }

        public async Task<string> GetVersion()
        {
            string cmd = LawicelCommands.v + "\r";
            byte[] bytes = Encoding.ASCII.GetBytes(cmd);

            await _dataStream.WriteData(bytes);
            var board = await _dataStream.ReadData(1000).ConfigureAwait(false);
            board = board.Replace("\r", " ");

            cmd = LawicelCommands.V + "\r";
            bytes = Encoding.ASCII.GetBytes(cmd);

            await _dataStream.WriteData(bytes);
            var version = await _dataStream.ReadData(1000).ConfigureAwait(false);
            version = version.Replace("\r", " ");

            cmd = LawicelCommands.N + "\r";
            bytes = Encoding.ASCII.GetBytes(cmd);

            await _dataStream.WriteData(bytes);
            var serial = await _dataStream.ReadData(1000).ConfigureAwait(false);
            serial = serial.Replace("\r", " ");

            return $"{board}-{version}-{serial}";
        }

        public async Task OpenChanel()
        {
            string cmd = LawicelCommands.O + "\r";
            await DevicePolling(cmd);
            _dataStream.StartDataStreaming();
        }

        public async Task CloseChanel()
        {
            string cmd = LawicelCommands.C + "\r";
            await DevicePolling(cmd);
            _dataStream.StartDataStreaming();
        }

        public Task Send11BitTxFrame(string data)
        {
            string cmd = LawicelCommands.t + data + "\r";
            return DevicePolling(cmd);
        }

        public Task Send29BitTxFrame(string data)
        {
            string cmd = LawicelCommands.T + data + "\r";
            return DevicePolling(cmd);
        }

        public Task SetDefaultBaudrate(string predefinedBaudrate)
        {
            string cmd = LawicelCommands.S + predefinedBaudrate + "\r";
            return DevicePolling(cmd);
        }

        public Task SetListenOnly()
        {
            string cmd = LawicelCommands.L + "\r";
            return DevicePolling(cmd);
        }

        public Task SetMaskFilter(string mask)
        {
            string cmd = LawicelCommands.m + mask + "\r";
            return DevicePolling(cmd);
        }

        public Task SetCodeFilter(string code)
        {
            string cmd = LawicelCommands.M + code + "\r";
            return DevicePolling(cmd);
        }

        public Task SetTimeStamp(bool isSendTimeStamp)
        {
            string lwvalue;

            if (isSendTimeStamp)
            {
                lwvalue = "1";
            }
            else
            {
                lwvalue = "0";
            }

            string cmd = LawicelCommands.Z + lwvalue + "\r";
            return DevicePolling(cmd);
        }

        private async Task DevicePolling(string cmd)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(cmd);

            await _dataStream.WriteData(bytes);
            var response = await _dataStream.ReadData(1000).ConfigureAwait(false);

            ValidateResponse(response);
        }

        private void ValidateResponse(string response)
        {
            if (response == LawicelCommands.BELL.ToString())
            {
                throw new InvalidOperationException("Invalid command.");
            }
        }

        private void DataStream_OnDataReceived(object sender, string e)
        {
            DataChannel.Writer.WriteAsync(e);
            PackageReceived?.Invoke(this, e);
        }
    }
}
