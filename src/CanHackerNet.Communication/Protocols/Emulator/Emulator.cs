﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Channels;
using System.Threading.Tasks;
using CanHackerNet.Communication.DataStreams.Core;
using CanHackerNet.Communication.Protocols.Core;

namespace CanHackerNet.Communication.Protocols.Emulator
{
    [ExcludeFromCodeCoverage]
    public class Emulator : ICan
    {
        private readonly IDataStream _dataStream;

        public Emulator(IDataStream dataStream)
        {
            _dataStream = dataStream;
        }

        public event EventHandler<string> PackageReceived;

        public Channel<string> DataChannel => throw new NotImplementedException();

        public Task CloseChanel()
        {
            throw new System.NotImplementedException();
        }

        public Task<string> GetVersion()
        {
            return Task.FromResult("v123");
        }

        public Task OpenChanel()
        {
            return Task.CompletedTask;
        }

        public Task Send11BitTxFrame(string data)
        {
            return Task.CompletedTask;
        }

        public Task Send29BitTxFrame(string data)
        {
            return Task.CompletedTask;
        }

        public Task SetCodeFilter(string code)
        {
            return Task.CompletedTask;
        }

        public Task SetDefaultBaudrate(string predefinedBaudrate)
        {
            return Task.CompletedTask;
        }

        public Task SetListenOnly()
        {
            return Task.CompletedTask;
        }

        public Task SetMaskFilter(string mask)
        {
            return Task.CompletedTask;
        }

        public Task SetTimeStamp(bool isSendTimeStamp)
        {
            return Task.CompletedTask;
        }
    }
}
