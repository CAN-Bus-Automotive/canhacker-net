﻿using System;
using System.Threading.Channels;
using System.Threading.Tasks;

namespace CanHackerNet.Communication.Protocols.Core
{
    public interface ICan
    {
        event EventHandler<string> PackageReceived;

        Channel<string> DataChannel { get; }

        Task<string> GetVersion();

        Task SetListenOnly();

        Task SetDefaultBaudrate(string predefinedBaudrate);

        Task OpenChanel();

        Task CloseChanel();

        Task SetTimeStamp(bool isSendTimeStamp);

        Task SetMaskFilter(string mask);

        Task SetCodeFilter(string code);

        Task Send11BitTxFrame(string data);

        Task Send29BitTxFrame(string data);
    }
}
