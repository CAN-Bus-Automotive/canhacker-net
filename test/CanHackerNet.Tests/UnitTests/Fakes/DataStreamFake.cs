﻿using CanHackerNet.Communication.DataStreams.Core;
using CanHackerNet.Communication.Protocols.Lawicel;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace CanHackerNet.Tests.UnitTests.Fakes
{
    public class DataStreamFake : IDataStream
    {
        private string _curretDataString = string.Empty;
        private byte[] _curretDataBytes = new byte[0];

        public bool IsDeviceConnected { get; set; }

        public bool IsDeviceResponding { get; set; }

        public bool IsDespondingOk { get; set; }

        public List<byte[]> IncomingCommands = new List<byte[]>();

        public event EventHandler<string> OnDataReceived;

        public string StreamName { get; set; }

        public Task CloseDataStream()
        {
            StreamName = string.Empty;

            return Task.CompletedTask;
        }

        public Task OpenDataSteram(string streamName)
        {
            StreamName = streamName;

            return Task.CompletedTask;
        }

        public Task<string> ReadData()
        {
            return Task.FromResult("TestResponse");
        }

        public Task WriteData(byte[] data)
        {
            if (!IsDeviceConnected)
            {
                throw new Exception();
            }

            IncomingCommands.Add(data);
            return Task.CompletedTask;
        }

        public Task<string> ReadData(int timeout)
        {
            if (!IsDeviceResponding)
            {
                throw new TimeoutException();
            }

            if (IsDespondingOk)
            {
                return Task.FromResult(LawicelCommands.CR.ToString());
            }

            return Task.FromResult(LawicelCommands.BELL.ToString());
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public void StartDataStreaming()
        {
        }

        public void StopDataStreaming()
        {
        }
    }
}
