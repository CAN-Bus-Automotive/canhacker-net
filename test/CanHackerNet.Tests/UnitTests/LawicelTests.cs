﻿using CanHackerNet.Communication.Protocols.Lawicel;
using CanHackerNet.Tests.UnitTests.Fakes;
using FluentAssertions;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;

namespace CanHackerNet.Tests.UnitTests
{
    public class LawicelTests
    {
        private DataStreamFake _dataStream;
        private Lawicel _lawicel;

        public LawicelTests()
        {
            _dataStream = new DataStreamFake();
            _lawicel = new Lawicel(_dataStream);
        }

        [Fact]
        public async Task GetVersion_IfDeviceConnected_ShouldReturnDeviceVersion()
        {
            // Arrange
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDespondingOk = true;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.v + "\r"));
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.V + "\r"));
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.N + "\r"));

            // Act
            var version = await _lawicel.GetVersion();

            // Assert
            Assert.Equal("13-13-13", version);
            _dataStream.IncomingCommands.Should().BeEquivalentTo(expList);
        }

        [Fact]
        public async Task GetVersion_IfDeviceConnected_ShouldThrowCorrectException()
        {
            // Arrange
            _dataStream.IsDeviceResponding = false;
            _dataStream.IsDeviceConnected = true;

            // Act & Assert
            var version = await Assert.ThrowsAsync<TimeoutException>(() => _lawicel.GetVersion());
        }

        [Fact]
        public async Task WritingAnyCommandToDevice_IfDeviceNotConnected_ShouldThrowException()
        {
            // Arrange
            _dataStream.IsDeviceResponding = false;

            // Act & Assert
            var version = await Assert.ThrowsAsync<Exception>(() => _lawicel.GetVersion());
        }

        [Fact]
        public async Task OpenChanel_IfDeviceConnectedAndResponding_ShouldSendValidCommand()
        {
            // Arrange
            _dataStream.IncomingCommands.Clear();
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = true;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.O + "\r"));


            // Act
            await _lawicel.OpenChanel();

            // Assert
            _dataStream.IncomingCommands.Should().BeEquivalentTo(expList);
        }

        [Fact]
        public async Task OpenChanel_IfDeviceConnectedAndWrongCommand_ShouldSendValidCommand()
        {
            // Arrange
            _dataStream.IncomingCommands.Clear();
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = false;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.O + "\r"));

            // Assert
            var version = await Assert.ThrowsAsync<InvalidOperationException>(() => _lawicel.OpenChanel());
        }

        [Fact]
        public async Task OpenChanel_IfDeviceConnectedAndNoRespondingOrInvalidCommand_ShouldThrowScecificException()
        {
            // Arrange
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = false;

            // Act & Assert
            var version = await Assert.ThrowsAsync<TimeoutException>(() => _lawicel.OpenChanel());
        }

        [Fact]
        public async Task ClooseChanel_IfDeviceConnectedAndResponding_ShouldSendValidCommand()
        {
            // Arrange
            _dataStream.IncomingCommands.Clear();
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = true;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.C + "\r"));

            // Act
            await _lawicel.CloseChanel();

            // Assert
            _dataStream.IncomingCommands.Should().BeEquivalentTo(expList);
        }

        [Fact]
        public async Task ClooseChanel_IfDeviceConnectedAndWrongCommand_ShouldSendValidCommand()
        {
            // Arrange
            _dataStream.IncomingCommands.Clear();
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = false;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.C + "\r"));

            // Act
            var version = await Assert.ThrowsAsync<InvalidOperationException>(() => _lawicel.CloseChanel());
        }

        [Fact]
        public async Task CloseChanel_IfDeviceConnectedAndNoResponding_ShouldThrowScecificException()
        {
            // Arrange
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = false;

            // Act & Assert
            var version = await Assert.ThrowsAsync<TimeoutException>(() => _lawicel.CloseChanel());
        }

        [Fact]
        public async Task SetDefaultBaudrate_IfDeviceConnectedAndResponding_ShouldSendValidCommand()
        {
            _dataStream.IncomingCommands.Clear();
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = true;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.S + "6" + "\r"));

            // Act
            await _lawicel.SetDefaultBaudrate("6");

            // Assert
            _dataStream.IncomingCommands.Should().BeEquivalentTo(expList);
        }

        [Fact]
        public async Task SetDefaultBaudrate_IfDeviceConnectedAndWrongCommand_ShouldThrowSpecificException()
        {
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = false;

            // Act & Assert
            var version = await Assert.ThrowsAsync<InvalidOperationException>(() => _lawicel.SetDefaultBaudrate("6"));
        }

        [Fact]
        public async Task SetListenOnly_IfDeviceConnectedAndResponding_ShouldSendValidCommand()
        {
            _dataStream.IncomingCommands.Clear();
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = true;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.L + "\r"));

            // Act
            await _lawicel.SetListenOnly();

            // Assert
            _dataStream.IncomingCommands.Should().BeEquivalentTo(expList);
        }

        [Fact]
        public async Task SetListenOnly_IfDeviceConnectedAndWrongCommand_ShouldThrowSpecificException()
        {
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = false;

            // Act & Assert
            var version = await Assert.ThrowsAsync<InvalidOperationException>(() => _lawicel.SetListenOnly());
        }

        [Fact]
        public async Task SetMaskFilter_IfDeviceConnectedAndResponding_ShouldSendValidCommand()
        {
            _dataStream.IncomingCommands.Clear();
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = true;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.m + "FFFF" + "\r"));

            // Act
            await _lawicel.SetMaskFilter("FFFF");

            // Assert
            _dataStream.IncomingCommands.Should().BeEquivalentTo(expList);
        }

        [Fact]
        public async Task SetMaskFilter_IfDeviceConnectedAndWrongCommand_ShouldThrowSpecificException()
        {
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = false;

            // Act & Assert
            var version = await Assert.ThrowsAsync<InvalidOperationException>(() => _lawicel.SetMaskFilter("FFFF-"));
        }

        [Fact]
        public async Task SetCodeFilter_IfDeviceConnectedAndResponding_ShouldSendValidCommand()
        {
            _dataStream.IncomingCommands.Clear();
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = true;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.M + "1111" + "\r"));

            // Act
            await _lawicel.SetCodeFilter("1111");

            // Assert
            _dataStream.IncomingCommands.Should().BeEquivalentTo(expList);
        }

        [Fact]
        public async Task SetCodeFilter_IfDeviceConnectedAndWrongCommand_ShouldThrowSpecificException()
        {
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = false;

            // Act & Assert
            var version = await Assert.ThrowsAsync<InvalidOperationException>(() => _lawicel.SetCodeFilter("1111-"));
        }

        [Theory]
        [InlineData(true, "1")]
        [InlineData(false, "0")]
        public async Task SetTimestamp_IfDeviceConnectedAndResponding_ShouldSendValidCommand(bool isSendTimeStamp, string lawicelValue)
        {
            _dataStream.IncomingCommands.Clear();
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = true;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.Z + lawicelValue + "\r"));

            // Act
            await _lawicel.SetTimeStamp(isSendTimeStamp);

            // Assert
            _dataStream.IncomingCommands.Should().BeEquivalentTo(expList);
        }

        [Fact]
        public async Task Send11BitTxFrame_IfDeviceConnectedAndResponding_ShouldSendValidCommand()
        {
            _dataStream.IncomingCommands.Clear();
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = true;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.t + "3112233" + "\r"));

            // Act
            await _lawicel.Send11BitTxFrame("3112233");

            // Assert
            _dataStream.IncomingCommands.Should().BeEquivalentTo(expList);
        }

        [Fact]
        public async Task Send29BitTxFrame_IfDeviceConnectedAndResponding_ShouldSendValidCommand()
        {
            _dataStream.IncomingCommands.Clear();
            _dataStream.IsDeviceConnected = true;
            _dataStream.IsDeviceResponding = true;
            _dataStream.IsDespondingOk = true;
            var expList = new List<byte[]>();
            expList.Add(Encoding.ASCII.GetBytes(LawicelCommands.T + "3112233" + "\r"));

            // Act
            await _lawicel.Send29BitTxFrame("3112233");

            // Assert
            _dataStream.IncomingCommands.Should().BeEquivalentTo(expList);
        }
    }
}
